﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace DL
{
    public class AccountHandler
    {
        private static readonly AccountHandler _instance;
        private static readonly object padlock = new object();

        static AccountHandler()
        {
            _instance = new AccountHandler();
        }

        public static AccountHandler Instance
        {
            get
            {
                lock (padlock)
                {
                    if (_instance == null) new AccountHandler();
                }
                return _instance;
            }
        }

        const string credentialFileName = "secret";

        public async Task<Account> GetAccount(string email)
        {
            StorageFolder f = ApplicationData.Current.LocalFolder;
            string fileContent = string.Empty;
            try
            {
                StorageFile credentialFile = await f.GetFileAsync(BuildFilename(email));
                Stream stream = await credentialFile.OpenStreamForReadAsync();

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Account));
                Account account = (Account)ser.ReadObject(stream);
                return account;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }

        private string BuildFilename(string email)
        {
            string emailNoDots = email.Replace('.', '_');
            return string.Format("{0}_{1}.txt", credentialFileName, emailNoDots);
        }

        public async Task<bool> SaveAccount(Account account)
        {
            return true;

            try
            {
                StorageFolder f = ApplicationData.Current.LocalFolder;
                string filename = BuildFilename(account.Email);
                string json = account.Jsonify();
                Stream stream = await f.OpenStreamForWriteAsync(filename, CreationCollisionOption.ReplaceExisting);
                byte[] writeme = Encoding.UTF8.GetBytes(json.ToCharArray(), 0, json.Length);
                stream.Write(writeme, 0, json.Length);
                return true;
            }
            catch (ArgumentNullException argNullEx)
            {
                return false;
            }
            catch (ArgumentException argEx)
            {
                return false;
            }
            catch (IOException ioEx)
            {
                return false;
            }
            catch (InvalidDataContractException invalidContract)
            {
                return false;
            }
            catch (SerializationException serialization)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
