﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Text.RegularExpressions;
using UI.classes;
using Model;
using DL;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UI.ViewModels
{
    public class NewAccountDlgViewModel : Bindable
    {

        public const string emailregex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";

        public delegate void dlgCreated();
        public event dlgCreated OnCreated;

        public NewAccountDlgViewModel()
        {

        }

        public string Email
        {
            get
            {
                return Get<string>();
            }
            set
            {
                Set<string>(value);
                OnPropertyChanged("CreateEnabled");
            }
        }

        public string Password1
        {
            get
            {
                return Get<string>();
            }
            set
            {
                Set<string>(value);
                OnPropertyChanged("CreateEnabled");
            }
        }

        public string Password2
        {
            get
            {
                return Get<string>();
            }
            set
            {
                Set<string>(value);
                OnPropertyChanged("CreateEnabled");
            }
        }

        public bool CreateEnabled
        {
            get
            {
                return ValidEmail && PasswordsMatch;
            }
        }


        private bool ValidEmail
        {
            get
            {
                if (string.IsNullOrEmpty(Email)) return false;

                return Regex.IsMatch(Email, emailregex);
            }
        }

        private bool PasswordsMatch
        {
            get
            {
                if (string.IsNullOrEmpty(Password1) || string.IsNullOrEmpty(Password2)) return false;
                return Password1.Equals(Password2, StringComparison.CurrentCulture);
            }
        }


        public ICommand CreateCommand
        {
            get
            {
                return new RelayCommand<object>(DoCreate);
            }
        }

        private async void DoCreate(object state)
        {
            try
            {
                Account account = new Account() { Email = this.Email, Password = this.Password1, CreatedDate = DateTime.Now };
                bool created = await AccountHandler.Instance.SaveAccount(account);
                //create account here
                if (created)
                {
                    OnCreated();
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }


        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand<object>(DoCancel);
            }
        }

        private void DoCancel(object state)
        {

        }
    }
}
