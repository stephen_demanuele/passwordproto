﻿using DL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using UI.classes;
using UI.Pages;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UI.ViewModels
{
    public class MainPageViewModel : Bindable
    {
        public const string emailregex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        
        public MainPageViewModel()
        {

        }

        public string Email
        {
            get
            {
                return Get<string>();
            }
            set
            {
                Set<string>(value);
                OnPropertyChanged("LoginEnabled");
            }
        }

        public string Password
        {
            get
            {
                return Get<string>();
            }
            set
            {
                Set<string>(value);
                OnPropertyChanged("LoginEnabled");
            }
        }

        private bool ValidEmail
        {
            get
            {
                if (string.IsNullOrEmpty(Email)) return false;

                return Regex.IsMatch(Email, emailregex);
            }
        }

        public bool LoginEnabled
        {
            get
            {
                return ValidEmail && !string.IsNullOrEmpty(Password);
            }
        }

        public bool ShowPassword
        {
            get
            {
                return Get<bool>();
            }
            set
            {
                Set<bool>(value);
            }
        }


        public string Status
        {
            get
            {
                return Get<string>();
            }
            private set
            {
                Set<string>(value);
            }
        }


        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand<object>(doLogin);
            }
        }

        private async void doLogin(object state)
        {
            Account account =  await AccountHandler.Instance.GetAccount(Email);
            if (account == null)
            {
                Status = "Login failed";
            }
            else
            {
                if (account.Password.Equals(Password, StringComparison.CurrentCulture))
                {
                    //go to list page
                    ListDlg listpage = new ListDlg();
                    var rootFrame = Window.Current.Content as Frame;
                    rootFrame.Navigate(typeof(ListDlg));
                }
                Status = "Login failed";
            }
        }

    }
}
