﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using UI.ViewModels;


namespace UI.Pages
{
    public sealed partial class NewAccountDlg : ContentDialog
    {
        public NewAccountDlg()
        {
            this.InitializeComponent();
            VM = new NewAccountDlgViewModel();
            VM.OnCreated += VM_OnCreated;
        }

        void VM_OnCreated()
        {
            MainPage mainPage = new MainPage();
            Frame currentFrame = Window.Current.Content as Frame;

            currentFrame.Navigate(typeof(MainPage));
        }

        public NewAccountDlgViewModel VM
        {
            get
            {
                return this.DataContext as NewAccountDlgViewModel;
            }
            private set
            {
                this.DataContext = value;
            }
        }


    }
}
