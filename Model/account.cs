﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace Model
{
    public class Account
    {

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Hint { get; set; }

        public string Jsonify()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Account));
            using (MemoryStream ms = new MemoryStream())
            {
                ser.WriteObject(ms, this);
                ms.Position = 0;
                StreamReader reader = new StreamReader(ms);
                return reader.ReadToEnd();
            }
        }

    }
}
